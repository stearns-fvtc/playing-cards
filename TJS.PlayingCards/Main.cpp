
// Playing Cards
// Tyler Stearns

#include <iostream>;
#include <conio.h>;

using namespace std;

enum class Suit 
{
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

enum class Rank
{
	ACE = 14,
	KING = 13,
	QUEEN = 12,
	JACK = 11,
	TEN = 10,
	NINE = 9,
	EIGHT = 8,
	SEVEN = 7,
	SIX = 6,
	FIVE = 5,
	FOUR = 4,
	THREE = 3,
	TWO = 2
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	Suit s1 = Suit::HEARTS;
	Suit s2 = Suit::CLUBS;
	Suit s3 = Suit::DIAMONDS;

	_getch();
	return 0;
}
